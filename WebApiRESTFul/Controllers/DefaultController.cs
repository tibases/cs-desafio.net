﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace WebApiRESTFul.Controllers
{
    [Authorize]
    [RoutePrefix("api/default")]
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route("datetime/")]
        public HttpResponseMessage GetDataHoraServidor()
        {
            try
            {
                var dataHora = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                return Request.CreateResponse(HttpStatusCode.OK, dataHora);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("authorize/")]
        // GET: Authorize Test
        public HttpResponseMessage Get()
        {

            try
            {
                //var user = HttpContext.Current.User;
                string message = HttpContext.Current.User.Identity.Name + " Message:Access Authorized";
                return Request.CreateResponse(HttpStatusCode.OK, new { message });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }


    }
}
