﻿using DAL.Model;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using Utils;
using WebApiRESTFul.Helper;
using WebApiRESTFul.Models;

namespace WebApiRESTFul.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {

        private UnitOfWork _unitOfWork = new UnitOfWork();
        private Repository<User> _userRepository;
        private Repository<Phone> _phoneRepository;

        [AllowAnonymous]
        [HttpPost]
        [Route("singup")]
        public HttpResponseMessage PostSingUp(UserModel userModel)
        {
            try
            {
                this._userRepository = this._unitOfWork.Repository<User>();

                if (this._userRepository.Table.Any(obj => obj.Email.ToLower() == userModel.Email))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Cadastro do usuário " + userModel.Name + " usuário já existe.");
                }

                //Insert User in database.
                var hash = new Hash(SHA512.Create());

                User user = new User()
                {
                    Name = userModel.Name,
                    Email = userModel.Email,
                    Password = hash.Cryptograph(userModel.Password),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    LastLogin = DateTime.Now
                };

                this._userRepository.Insert(user);

                //Insert Phones 
                if (userModel.Phones != null && userModel.Phones.Count() > 0)
                {
                    _phoneRepository = this._unitOfWork.Repository<Phone>();

                    foreach (var phone in userModel.Phones)
                    {
                        _phoneRepository.Insert(new Phone()
                        {
                            DDD = phone.DDD,
                            Number = phone.Number,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,

                            User = user,
                        });
                    }
                }

                userModel.ID = userModel.ID;

                return Request.CreateResponse(HttpStatusCode.OK, "Cadastro do usuário " + userModel.Name + " realizado.");
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("singin")]
        public HttpResponseMessage PostSingIn(UserModel userModel)
        {
            HttpResponseMessage response = null;

            try
            {
                if (ModelState.IsValid)
                {
                    this._userRepository = this._unitOfWork.Repository<User>();

                    User user = this._userRepository.Table.Where(obj => obj.Email.ToLower() == userModel.Email).FirstOrDefault();


                    if (user != null)
                    {
                        var hash = new Hash(SHA512.Create());

                        if (hash.PasswordValidate(userModel.Password, user.Password))
                        {
                            userModel.Name = user.Name;
                            userModel.ID = user.ID;
                            userModel.Phones = user.Phones.Select(obj => new PhoneModel() { DDD = obj.DDD, Number = obj.Number });

                            //Updated login date
                            user.LastLogin = DateTime.Now;
                            this._userRepository.Update(user);

                            //Generate Token    
                            object dbUser;
                            var token = Token.CreateToken(user, out dbUser);
                            response = Request.CreateResponse(new { dbUser, token });
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

            return response;
        }

    }
}
