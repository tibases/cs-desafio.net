﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApiRESTFul.Controllers;
using System.Net.Http;
using System.Web.Http;
using WebApiRESTFul.Models;
using System.Net;
using DAL.Repository;
using DAL.Model;
using Utils;
using System.Security.Cryptography;
using System;
using System.Linq;
using Newtonsoft.Json;
using JWT;

namespace UnityTestProject
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void SingUpTest()
        {
            var controller = new UserController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            var userModel = new UserModel
            {
                Name = this._user.Name,
                Email = this._user.Email,
                Password = "123456"
            };

            var response = controller.PostSingUp(userModel);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

            this.CleanDataBase(userModel);
        }

        [TestMethod]
        public void SingUpExistUserTest()
        {
            var controller = new UserController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            this.InsertDataBase(this._user);

            var userModel = new UserModel
            {
                Name = this._user.Name,
                Email = this._user.Email,
                Password = "123456"
            };

            var response = controller.PostSingUp(userModel);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

            this.CleanDataBase(userModel);
        }


        [TestMethod]
        public void SingInTest()
        {
            var controller = new UserController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            UserModel userModel = new UserModel()
            {
                Email = this._user.Email,
                Password = "123556"
            };

            this.InsertDataBase(this._user);

            var response = controller.PostSingIn(userModel);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

            this.CleanDataBase(userModel);
        }


        [TestMethod]
        public void TokenTest()
        {
            var controller = new UserController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            UserModel userModel = new UserModel()
            {
                Email = this._user.Email,
                Password = "123556"
            };

            this.InsertDataBase(this._user);

            //With Token
            var response = controller.PostSingIn(userModel);
            string jsonContent = response.Content.ReadAsStringAsync().Result;
            UserModel userModelJson = JsonConvert.DeserializeObject<UserModel>(jsonContent);

            var payloadJson = JsonWebToken.Decode(userModelJson.Token, "secretKey");
            var payloadObject = JsonConvert.DeserializeObject<UserModel>(payloadJson);

            Assert.IsTrue(payloadObject.Name == this._user.Name);
            Assert.IsTrue(payloadObject.Email == this._user.Email);

            this.CleanDataBase(userModel);
        }







        #region Private Methods

        private UnitOfWork _unitOfWork = new UnitOfWork();
        private Repository<User> _userRepository;

        private void CleanDataBase(UserModel userModel)
        {
            this._userRepository = this._unitOfWork.Repository<User>();
            if (userModel.ID != Guid.Empty)
            {
                this._userRepository.Delete(this._userRepository.GetById(userModel.ID));
            }
            else
            {
                this._userRepository.Delete(this._userRepository.Table.Where(obj => obj.Email.ToLower() == userModel.Email.ToLower()).First());
            }


        }


        private void InsertDataBase(User userModel)
        {
            this._userRepository = this._unitOfWork.Repository<User>();
            this._userRepository.Insert(this._user);
        }


        public User _user
        {
            get
            {
                //Insert User in database.
                var hash = new Hash(SHA512.Create());

                return new User()
                {
                    Name = "Test",
                    Email = "email123456123463214@test.com.br",
                    Password = hash.Cryptograph("123556"),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    LastLogin = DateTime.Now
                };
            }
        }


        #endregion
    }
}
